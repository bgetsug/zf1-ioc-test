<?php

// include composer autoloader
require_once(__DIR__ . '/../vendor/autoload.php');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

// Ensure library/ is on include_path
//set_include_path(implode(PATH_SEPARATOR, array(
//    realpath(APPLICATION_PATH . '/../library'),
//    get_include_path(),
//)));


/** Zend_Application */
//require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

//$autoLoaders = spl_autoload_functions();

spl_autoload_unregister(array('Zend_Loader_Autoloader','autoload'));

Zend_Registry::set('container', require_once(__DIR__ . '/../library/container.php'));

$application->bootstrap()
            ->run();