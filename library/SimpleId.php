<?php

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Exception\IDPException as IDPException;
use Guzzle\Http\Exception\BadResponseException;


class SimpleId extends AbstractProvider
{

    public function urlAuthorize()
    {
        return 'http://id.app/oauth/authorize';
    }

    public function urlAccessToken()
    {
        return 'http://id.app/oauth/token';
    }

    public function urlUserDetails(AccessToken $token)
    {
        $this->headers = array(
            'Authorization' => "Bearer $token"
        );

        return 'http://id.app/oauth/user';
    }

    public function userDetails($response, AccessToken $token)
    {
        return $response->username;
    }

    protected function fetchUserDetails(AccessToken $token)
    {
        $url = $this->urlUserDetails($token);

        try {

            $client = $this->getHttpClient();
            $client->setBaseUrl($url);

            if ($this->headers) {
                $client->setDefaultOption('headers', $this->headers);
            }

            $request = $client->post()->send();
            $response = $request->getBody();

        } catch (BadResponseException $e) {
            // @codeCoverageIgnoreStart
            $raw_response = explode("\n", $e->getResponse());
            throw new IDPException(end($raw_response));
            // @codeCoverageIgnoreEnd
        }

        return $response;
    }
}
