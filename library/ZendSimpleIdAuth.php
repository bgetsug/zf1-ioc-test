<?php

class ZendSimpleIdAuth implements Zend_Auth_Adapter_Interface {

    /**
     * $_username - Identity value
     *
     * @var string
     */
    protected $_username;


    public function __construct($username)
    {
        $this->_username = $username;
    }


    /**
     * Performs an authentication attempt
     *
     * @throws Zend_Auth_Adapter_Exception If authentication cannot be performed
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this->_username);
    }
} 