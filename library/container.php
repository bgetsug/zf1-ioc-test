<?php

use Foo\Foo;

$container = new Illuminate\Container\Container();

// bindings
$container->bind('foo', function() {
		return new Foo;
	}
);

return $container;