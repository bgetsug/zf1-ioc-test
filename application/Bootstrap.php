<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    public function __construct($application)
    {
        parent::__construct($application);
        ErrorHandler::set();
    }


    protected function _initMonolog()
    {
        $log = new Logger('zf1test');
        $log->pushHandler(new StreamHandler('/tmp/zf1test.log'));

        Zend_Registry::set('log', $log);
    }

    protected function _initWhoops()
    {
        if (APPLICATION_ENV == 'development') {
            $this->bootstrap('monolog');

            // this line forces Zend Framework to throw exceptions and let us handle them
            Zend_Controller_Front::getInstance()->throwExceptions(true);

            // create new Whoops object
            $whoops = new Whoops\Run();

            // adding an error handler, pretty one ;)
            $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler());
            //$whoops->pushHandler(new Whoops\Handler\JsonResponseHandler);

            $logger = Zend_Registry::get('log');

            // Place our custom handler in front of the others, capturing exceptions
            // and logging them, then passing the exception on to the other handlers:
            $whoops->pushHandler(function ($exception, $inspector, $run) use ($logger) {
                if ($exception instanceof Exception && $logger instanceof Logger) {
                    $logger->addCritical($exception->getMessage() . PHP_EOL . $exception->getTraceAsString());
                }
            });

            // set Whoops as default error handler
            $whoops->register();
        }
    }

    /**
     * Start session
     */
    protected function _initSession()
    {
        if (PHP_SAPI != 'cli') {

            $config = $this->getOptions();
            $options = $config['resources']['session'];

            Zend_Session::setOptions($options);
            Zend_Session::start();

            $session = new Zend_Session_Namespace();
            Zend_Registry::set('session', $session);
        }

    }
}

