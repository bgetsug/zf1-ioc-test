<?php

class AuthController extends Zend_Controller_Action
{
    /**
     * @var Monolog\Logger
     */
    private $_logger;

    /**
     * @var Zend_Session
     */
    private $_session;

    /**
     * @var array
     */
    protected $providerConfig = array(
        'clientId'  =>  'testclient3',
        'clientSecret'  =>  'testpass3',
        'redirectUri'   =>  'http://zf1.app/auth'
    );

    /**
     * @var SimpleId
     */
    protected $provider;


    public function init()
    {
        $this->_logger = Zend_Registry::get('log');
        $this->_session = Zend_Registry::get('session');

        $this->provider = new SimpleId($this->providerConfig);
    }

    
    public function indexAction()
    {
        $code = $this->_request->getParam('code');

        if ( ! isset($code)) {
            // If we don't have an authorization code then get one
            $this->_logger->debug('auth: redirect to authorization URL...');
            $this->redirect($this->provider->getAuthorizationUrl());
        }

        $this->_logger->debug('auth - code: ' . $code);
        $this->_logger->debug('auth - state: ' . $this->_request->getParam('state'));


        // Try to get an access token (using the authorization code grant)
        $token = $this->provider->getAccessToken('authorization_code', [
            'code' => $code,
            'grant_type' => 'authorization_code'
        ]);

        $this->_logger->debug("auth - token: $token");

        // get user info with access token and make a User
        try {

            // We got an access token, let's now get the user's details
            $username = $this->provider->getUserDetails($token);

            // Zend_Auth
            $adapter = new ZendSimpleIdAuth($username);

            $auth = Zend_Auth::getInstance();
            $auth->authenticate($adapter);

            $user = new stdClass();
            $user->username = $username;

            $auth->getStorage()->write($user);

            $this->_logger->debug("auth - we're logged in...redirecting to 'private'...");
            $this->redirect('/private');

        } catch (Exception $e) {

            // Failed to get user details
            $this->_helper->json(array('error' => $e->getMessage()));
        }

    }
}





