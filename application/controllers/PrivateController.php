<?php

class PrivateController extends Zend_Controller_Action
{

    /**
     * @var mixed
     */
    protected $_identity;

    /**
     * @var string
     */
    protected $_username;

    public function preDispatch()
    {
        if (PHP_SAPI != 'cli') {

            if (!Zend_Auth::getInstance()->hasIdentity()) {
                //$this->redirect('/auth');
                $this->redirect('/sso/login');
            } else {
                $this->_identity = Zend_Auth::getInstance()->getIdentity();
                //Logger::debug('$this->_identity: ' . print_r($this->_identity, true));
                $this->_username = $this->_identity->username;
                //Logger::debug("Username is $this->_username");
            }

            // prevents session locks from blocking simultaneous (ajax) requests
            Zend_Session::writeClose();
        }
    }

    public function indexAction()
    {
        $this->_helper->json("Logged in as: $this->_username");
    }
}

