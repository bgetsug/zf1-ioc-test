<?php

class SsoController extends Zend_Controller_Action
{
    /**
     * @var Monolog\Logger
     */
    private $_logger;

    /**
     * @var Zend_Session
     */
    private $_session;

    /**
     * @var \Tymon\JWTAuth\Providers\FirebaseProvider
     */
    private $_jwtAuth;


    public function init()
    {
        $this->_logger = Zend_Registry::get('log');
        $this->_session = Zend_Registry::get('session');

        $secret = $this->getInvokeArg('bootstrap')->getOption('jwt')['shared_secret'];
        $request = \Illuminate\Http\Request::createFromBase(\Symfony\Component\HttpFoundation\Request::createFromGlobals());

        $this->_jwtAuth = new \Tymon\JWTAuth\Providers\FirebaseProvider($secret, $request);
    }


    public function loginAction()
    {
        $token = $this->_request->getParam('token');

        if ( ! isset($token)) {
            // if we don't have a token then get one
            $this->_logger->debug('login: redirect to authorization URL...');
            //todo get sso login url from config
            $this->redirect("http://id.app/sso/login?appName=zf1");
        }

        // get user info with access token and make a User
        try {

            $username = $this->_jwtAuth->getSubject($token);

            // Zend_Auth
            $adapter = new ZendSimpleIdAuth($username);

            $auth = Zend_Auth::getInstance();
            $auth->authenticate($adapter);

            $user = new stdClass();
            $user->username = $username;

            $auth->getStorage()->write($user);

            $this->_logger->debug("login - we're logged in...redirecting to 'private'...");
            $this->redirect('/private');

        } catch (Exception $e) {

            // Failed to get user details
            $this->_helper->json(array('error' => $e->getMessage()));
        }

    }


    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();

        // remove the entire session, not just the Zend_Auth namespace
        Zend_Session::destroy();

        //$this->redirect('http://id.app/logout');
        $this->redirect('http://id.app/sso/logout?appName=zf1');
    }
}





