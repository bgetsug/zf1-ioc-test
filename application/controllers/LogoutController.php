<?php

class LogoutController extends Zend_Controller_Action
{

    public function indexAction()
    {
        Zend_Auth::getInstance()->clearIdentity();

        // remove the entire session, not just the Zend_Auth namespace
        Zend_Session::destroy();

        $this->redirect('http://id.app/logout');
    }
}

