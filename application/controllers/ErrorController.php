<?php

use Monolog\Logger;

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        
        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Logger::NOTICE;
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Logger::CRITICAL;
                $this->view->message = 'Application error';
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
	        $context = array(
		        'server' => $_SERVER,
	            'get' => $_GET,
	            'post' => $_POST,
	            'files' => $_FILES,
	            'cookie' => $_COOKIE,
	            'session' => isset($_SESSION) ? $_SESSION : array(),
	            'env' => $_ENV
 	        );
            $log->log($priority, $errors->exception->getMessage() . PHP_EOL . $errors->exception->getTraceAsString(), $context);
            //$log->log($priority, 'Request Parameters', $errors->request->getParams());
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
    }


	/**
	 * @return bool|Logger
	 */
	public function getLog()
    {
//        $bootstrap = $this->getInvokeArg('bootstrap');
//        if (!$bootstrap->hasResource('Log')) {
//            return false;
//        }
//        $log = $bootstrap->getResource('Log');
	    if ( ! Zend_Registry::isRegistered('log')) {
		    return false;
	    }

	    $log = Zend_Registry::get('log');

        return $log;
    }


}

