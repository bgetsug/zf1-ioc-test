<?php
use \Mockery as m;

class IndexControllerTest extends Zend_Test_PHPUnit_ControllerTestCase
{

	/**
	 * @var \Illuminate\Container\Container
	 */
	protected $c;

    public function setUp()
    {
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');

	    spl_autoload_unregister(array('Zend_Loader_Autoloader','autoload'));

	    $this->c = require_once(__DIR__ . '/../../../library/container.php');
	    $this->bootstrap->getBootstrap()->setContainer($this->c);

        parent::setUp();
    }

	public function testIndexAction()
	{
		$mock = m::mock('Foo\Foo');
		$mock->shouldReceive('bar')->once()->andReturn('bar');
		$this->c['foo'] = function() use ($mock) {
			return $mock;
		};

		$params = array('action' => 'index', 'controller' => 'Index', 'module' => 'default');
		$urlParams = $this->urlizeOptions($params);
		$url = $this->url($urlParams);
		$this->dispatch($url);

		$this->assertModule($urlParams['module']);
		$this->assertController($urlParams['controller']);
		$this->assertAction($urlParams['action']);

		$this->assertContains('bar', json_decode($this->getResponse()->getBody()));
	}
}

